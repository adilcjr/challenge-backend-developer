<p align="center">
  <a href="https://www.btc-banco.com">
      <img src="https://s3.amazonaws.com/assinaturas-de-emails/btc.png" alt="Grupo Bitcoin Banco"/>
  </a>
</p>

## Challenge for Developer

### Developer Level
- Mid-level
- Senior

A customer needs to search in our product catalog (available in this <a href="https://github.com/Bitcoin-Banco-Cryptocurrency/challenge/blob/master/books.json">JSON</a>) and he wants to find products that suit his style of reading.
Based on this you will need to develop:

- a simple API to search products in the .json available;
- it should be possible to search for products by their specifications (one or more);
- it must be possible to order the result by price (asc and desc);

The test should be done in Ruby, Go, Python or Node and we do like if you avoid frameworks. We expect at the end of the test, outside the API running, the following items:

- an explanation of what is needed to make your project work;
- an explanation of how to perform the tests

Remember that at the time of the evaluation we will look at:

- Code organization;
- Object-Oriented Principles;
- Maintenance;
- Version control knowledge;
- Unit Test;
- Design Pattern;

To send us your code, you must:

Make a fork of this repository, and send us a pull-request.

## API Documentation

The application was built using NodeJs and ExpressJS. booksAPI.js is the main script and call a controller book.controller.js.
This Controller delegates the request to the right Book function to deal with data in model layer.
Model layer makes some magic to bring the right information.

There is a detailed explanation for rest calls in book.controller. 
It's build in apiDoc format, but this framework wasn't installed to let application more cleanned.

### Install Instructions

To run this API:

> - Go to application folder
> - Call node booksAPI, in command prompt
> - When the message "Server listen on port 8000!" is shown the server will be ready
> - On your browser or REST Application test call the REST requests

### REST Request Documentation

> - {get} localhost:8000/api/books/ Request All Books
> - {get} localhost:8000/api/books/id/:id Request Book by Id
> - {get} localhost:8000/api/books/name/:name/:order? Find Book by name, optionally order by price (ASC/DESC)
> - {get} localhost:8000/api/books/spec/:key/:value Find Book by specification

### To Do

> - Unit Test
> - Improve error flow
