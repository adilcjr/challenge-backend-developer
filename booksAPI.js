var express = require('express');
var parser = require('body-parser');

var app = express();
app.use(parser.json());
app.use(parser.urlencoded({extended: false}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET");
    next();
});

app.use((err, req, res, next) => {
	console.log('Requisition Failed!');
	res.status(500).send(err.message);
});

var bookAPI = require("./controllers/book.controller");

app.use('/api/books', bookAPI);

app.listen(8000);
console.log("Server listen on port 8000!");