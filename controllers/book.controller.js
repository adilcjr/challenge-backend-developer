
var Books = require('../model/books.js');
var books = new Books();

var express = require('express');
var router = express.Router();


/**
 * @api {get} / Request All Books
 * @apiName getAllBooks
 * @apiGroup Books
 * 
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *     [
 *       {
 *          "id": 1,
 *          "name": "Journey to the Center of the Earth",
 *          "price": 10,
 *          "specifications": {
 *              "Originally published": "November 25, 1864",
 *              "Author": "Jules Verne",
 *              "Page count": 183,
 *              "Illustrator": "Édouard Riou",
 *              "Genres": [
 *                  "Science Fiction",
 *                  "Adventure fiction"
 *              ]
 *          }
 *       }
 *     ]
 */
router.get("/", (req, res) => {
    res.send(books.getAllBooks());
});


/**
 * @api {get} /id/:id Request Book By ID
 * @apiName getBook
 * @apiGroup Books
 *
 * @apiParam {id} id Book unique ID.
 *
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *     [
 *       {
 *          "id": 1,
 *          "name": "Journey to the Center of the Earth",
 *          "price": 10,
 *          "specifications": {
 *              "Originally published": "November 25, 1864",
 *              "Author": "Jules Verne",
 *              "Page count": 183,
 *              "Illustrator": "Édouard Riou",
 *              "Genres": [
 *                  "Science Fiction",
 *                  "Adventure fiction"
 *              ]
 *          }
 *       }
 *     ]
 */
router.get("/id/:id", (req, res) => {
    res.send(books.findById(req.params.id));
});

/**
 * @api {get} /name/:name/:order Find Books By Name, optionally order by price
 * @apiName findBookByName
 * @apiGroup Books
 *
 * @apiParam {name} name part of the book name.
 * @apiParam {orcer} order ASC for ascending by price, DESC otherwise.
 *
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *     [
 *       {
 *          "id": 1,
 *          "name": "Journey to the Center of the Earth",
 *          "price": 10,
 *          "specifications": {
 *              "Originally published": "November 25, 1864",
 *              "Author": "Jules Verne",
 *              "Page count": 183,
 *              "Illustrator": "Édouard Riou",
 *              "Genres": [
 *                  "Science Fiction",
 *                  "Adventure fiction"
 *              ]
 *          }
 *       }
 *     ]
 */
router.get("/name/:name/:order?", (req, res) => {  
    res.send(books.findByName(req.params.name, req.params.order));
});

/**
 * @api {get} /spec/:key/:value Find Books By Specification
 * @apiName findBySpecification
 * @apiGroup Books
 *
 * @apiParam {key} specification key.
 * @apiParam {value} value part of the specification value.
 *
 * @apiSuccessExample Success-Response:
 *   HTTP/1.1 200 OK
 *     [
 *       {
 *          "id": 1,
 *          "name": "Journey to the Center of the Earth",
 *          "price": 10,
 *          "specifications": {
 *              "Originally published": "November 25, 1864",
 *              "Author": "Jules Verne",
 *              "Page count": 183,
 *              "Illustrator": "Édouard Riou",
 *              "Genres": [
 *                  "Science Fiction",
 *                  "Adventure fiction"
 *              ]
 *          }
 *       }
 *     ]
 */
router.get("/spec/:key/:value", (req, res) => {
    res.send(books.findBySpecification(req.params.key, req.params.value));
});

module.exports = router;