var books;

var Books = function() {

    const fs = require('fs');
    var data = fs.readFileSync('books.json');
    books = JSON.parse(data);
};

Books.prototype.getAllBooks = function() {
    return books;
};

Books.prototype.findByName =  function(name, order) {   
    return books.filter(        
        function(book) {            
            return book.name.indexOf(name) >= 0;
        })
        .sort( function (a, b) {
            if (order == 'ASC') {
                return a.price > b.price;
            } else if (order == 'DESC') {
                return a.price < b.price;
            }
        }
    );
}

Books.prototype.findById = function(id) {
    return books.filter(
        function(book) {
            return book.id == id;
        });
}

Books.prototype.findBySpecification = function(key, value) {
    return books.filter(
        function(book) {
            return book.specifications[key].indexOf(value) >= 0;
        });
}

module.exports = Books;